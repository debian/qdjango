qdjango (0.6.2-3.3) unstable; urgency=medium

  * debian/control:
    + Move doxygen from B-D-I: to B-D: field.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 16 Feb 2022 22:51:48 +0100

qdjango (0.6.2-3.2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control:
    + Removing redundant Priority: field in binary package.

  [ Jelmer Vernooĳ ]
  * Transition to automatic debug package (from: libqdjango-dbg).

  [ Mike Gabriel ]
  * Non-maintainer upload.
  * debian/watch:
    + Fix / adjust watch URL. Switch to format version 4.
  * debian/rules:
    + Adjust get-orig-source target, stop using wget.
    + Fix binary-only builds. (Closes: #980267).

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 15 Feb 2022 08:51:41 +0100

qdjango (0.6.2-3.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 09 Jan 2021 16:11:19 +0100

qdjango (0.6.2-3) unstable; urgency=medium

  * Update Standards-Version to 4.2.1.
  * Update VCS links to point to salsa.debian.org.

 -- Jeremy Lainé <jeremy.laine@m4x.org>  Sat, 08 Sep 2018 18:36:40 +0200

qdjango (0.6.2-2) unstable; urgency=medium

  * Fix testsuite build with GCC 6 (Closes: #812000).

 -- Jeremy Lainé <jeremy.laine@m4x.org>  Tue, 21 Jun 2016 08:47:30 +0200

qdjango (0.6.2-1) unstable; urgency=medium

  * New upstream release (Closes: #799659).

 -- Jeremy Lainé <jeremy.laine@m4x.org>  Thu, 22 Oct 2015 11:32:01 +0200

qdjango (0.6.1-2) unstable; urgency=medium

  * Disable tests making platform-dependent assumptions about hashing.

 -- Jeremy Lainé <jeremy.laine@m4x.org>  Thu, 13 Aug 2015 18:55:37 +0200

qdjango (0.6.1-1) unstable; urgency=medium

  * New upstream release.

 -- Jeremy Lainé <jeremy.laine@m4x.org>  Thu, 13 Aug 2015 10:02:17 +0200

qdjango (0.6.0-1) unstable; urgency=medium

  * New upstream release (Closes: #789405).
  * Remove obsolete libqdjango-script0 binary package.
  * Build against Qt5.
  * Add Build-Depends on libqt5sql5-sqlite to run unit tests.
  * Update Standards-Version to 3.9.6 (no changes).
  * Update Vcs-* fields to point to git.

 -- Jeremy Lainé <jeremy.laine@m4x.org>  Thu, 13 Aug 2015 09:51:38 +0200

qdjango (0.5.0-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards-Version to 3.9.5 (no changes).
  * Update homepage to point to github.
  * Update debian/watch file for github.

 -- Jeremy Lainé <jeremy.laine@m4x.org>  Sat, 13 Sep 2014 16:24:48 +0200

qdjango (0.4.0-1) unstable; urgency=low

  * New upstream release.

 -- Jeremy Lainé <jeremy.laine@m4x.org>  Fri, 28 Jun 2013 18:09:27 +0200

qdjango (0.3.0-1) unstable; urgency=low

  * New upstream release (Closes: #714142).
  * Update Standards-Version to 3.9.4 (no changes).
  * Change Vcs-* fields to point to canonical urls.

 -- Jeremy Lainé <jeremy.laine@m4x.org>  Wed, 26 Jun 2013 10:56:33 +0200

qdjango (0.2.6-1) unstable; urgency=low

  * New upstream release.
  * Update debian/rules:
    - add get-orig-source target.
    - remove override_dh_install, it is not necessary.
  * Provide debugging symbols in libqdjango-dbg (Closes: #686888).
  * debian/libqdjango-doc.links: fix lintian notes about duplicate files.

 -- Jeremy Lainé <jeremy.laine@m4x.org>  Fri, 07 Sep 2012 11:37:07 +0200

qdjango (0.2.5-3) unstable; urgency=low

  * Add debian/watch.
  * Add debian/libqdjango-doc.doc-base.
  * Fix section for libqdjango-(db|http|script)0 packages.
  * Fix libqdjango-doc documentation directory.

 -- Jeremy Lainé <jeremy.laine@m4x.org>  Fri, 24 Aug 2012 10:03:57 +0200

qdjango (0.2.5-2) unstable; urgency=low

  * Add multi-arch support.

 -- Jeremy Lainé <jeremy.laine@m4x.org>  Thu, 31 May 2012 19:12:25 +0200

qdjango (0.2.5-1) unstable; urgency=low

  * Initial upload to Debian (Closes: #671779).

 -- Jeremy Lainé <jeremy.laine@m4x.org>  Wed, 16 May 2012 18:13:49 +0200

